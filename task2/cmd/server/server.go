package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net/http"

	"os/signal"
	"syscall"

	"golang.org/x/sync/errgroup"

	"mts_go_hw/task2/internal/config"
	"mts_go_hw/task2/internal/http-server/handlers/get"
	"mts_go_hw/task2/internal/http-server/handlers/post"
)

const shutdownTimeout = 30

func main() {
	cfg, err := ReadConfig()
	if err != nil {
		return
	}

	var (
		mux = http.NewServeMux()
		srv = &http.Server{
			Addr:    cfg.Server.Address,
			Handler: mux,
		}
	)

	mux.HandleFunc("/version", get.ReturnApiVersion)
	mux.HandleFunc("/decode", post.ProcessString)
	mux.HandleFunc("/hard-op", get.ProcessHardOp)

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	group, ctx := errgroup.WithContext(ctx)

	group.Go(func() (err error) {
		if err = srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			return fmt.Errorf("failed to serve http-server: %w", err)
		}
		return
	})

	group.Go(func() (err error) {
		<-ctx.Done()
		shutdownCtx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		err = srv.Shutdown(shutdownCtx)
		if err != nil {
			return
		}
		return
	})

	err = group.Wait()
	if err != nil {
		fmt.Printf("after wait: %s\n", err)
		return
	}
}

func ReadConfig() (*config.Config, error) {
	var configPath string
	flag.StringVar(&configPath, "c", ".config.yaml", "set config path")
	flag.Parse()
	return config.NewConfig(configPath)
}
