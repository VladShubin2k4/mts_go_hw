package main

import (
	"mts_go_hw/task2/internal/client"
	"net/http"
	"time"
)

func main() {
	urls := []string{
		"http://localhost:8080/version",
		"http://localhost:8080/decode",
		"http://localhost:8080/hard-op",
	}
	c := client.NewClient(&http.Client{Timeout: time.Duration(15) * time.Second}, urls)
	c.SendRequests()
}
