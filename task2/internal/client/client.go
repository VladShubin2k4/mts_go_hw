package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"mts_go_hw/task2/internal/lib/random"
	"net/http"
)

type ResponseReport struct {
	StatusCode int
	Success    bool
}

type PostRequest struct {
	InputString string `json:"inputString"`
}

type Client struct {
	client       *http.Client
	responseUrls []string
}

func NewClient(client *http.Client, urls []string) Client {
	return Client{
		client:       client,
		responseUrls: urls,
	}
}

func (c *Client) AddUrl(url string) {
	c.responseUrls = append(c.responseUrls, url)
}

func (c *Client) SendRequests() {
	var err error = nil
	var rep ResponseReport
	for i := 0; err == nil; i++ {
		i %= len(c.responseUrls)
		switch i {
		case 1:
			err, rep = c.SendPostRequest(i)
		default:
			err, rep = c.SendGetRequest(i)
		}
		if err != nil {
			fmt.Println(rep.Success, rep.StatusCode)
		}
	}
}

func (c *Client) SendPostRequest(ind int) (err error, rep ResponseReport) {
	req := PostRequest{InputString: random.NewRandomBase64String(rand.Int() % 128)}
	jsonReq, err := json.Marshal(req)
	if err != nil {
		return
	}
	resp, err := c.client.Post((c.responseUrls)[ind], "application/json", bytes.NewBuffer(jsonReq))
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusRequestTimeout
	}
	if err != nil {
		rep.Success = false
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func (c *Client) SendGetRequest(ind int) (err error, rep ResponseReport) {
	resp, err := c.client.Get((c.responseUrls)[ind])
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusRequestTimeout
	}
	if err != nil {
		fmt.Printf("Error %s\n", err)
		rep.Success = false
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}
