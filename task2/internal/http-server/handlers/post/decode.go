package post

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Request struct {
	InputString string `json:"inputString"`
}

type Response struct {
	Status        string `json:"status"`
	Error         error  `json:"error"`
	DecodedString string `json:"outputString"`
}

func DecodingBase64(inputString string) (string, error) {
	decodeString, err := base64.StdEncoding.DecodeString(inputString)
	return string(decodeString), err
}

func ProcessString(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal("failed to close request body")
		}
	}(r.Body)

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Fatal("failed to read request body")
	}

	var req Request
	err = json.Unmarshal(reqBody, &req)
	if err != nil {
		log.Fatal("failed to unmarshal request")
	}

	decodedString, err := DecodingBase64(req.InputString)
	if err != nil {
		log.Fatal("failed to post inputString")
	}

	ans, err := json.Marshal(Response{
		Status:        "OK",
		Error:         err,
		DecodedString: decodedString,
	})
	if err != nil {
		log.Fatal("failed to Marshal json")
	}

	_, err = w.Write(ans)
	if err != nil {
		log.Fatal("failed write")
	}
	return
}
