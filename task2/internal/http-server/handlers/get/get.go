package get

import (
	"math/rand"
	"net/http"
	"time"
)

func ReturnApiVersion(w http.ResponseWriter, _ *http.Request) {
	_, err := w.Write([]byte("v0.0.1"))
	if err != nil {
		return
	}
}

func ProcessHardOp(w http.ResponseWriter, _ *http.Request) {
	switch rand.Intn(2) {
	case 0:
		w.WriteHeader(http.StatusInternalServerError)
	case 1:
		w.WriteHeader(http.StatusOK)
	}
	time.Sleep(time.Duration(10+rand.Intn(10+1)) * time.Second)
}
