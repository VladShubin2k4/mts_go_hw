package config

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
	"time"
)

type Config struct {
	StoragePath string     `yaml:"storage_path"`
	Server      HTTPServer `yaml:"http_server"`
}

type HTTPServer struct {
	Address     string        `yaml:"address"`
	Timeout     time.Duration `yaml:"timeout"`
	IdleTimeout time.Duration `yaml:"idle_timeout"`
}

func NewConfig(configPath string) (cfg *Config, err error) {
	bytes, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatal("configPath variable is not set")
		return
	}

	cfg = &Config{}
	err = yaml.Unmarshal(bytes, cfg)
	if err != nil {
		return
	}

	return
}
