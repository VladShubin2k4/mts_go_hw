module mts_go_hw

go 1.21

require (
	golang.org/x/sync v0.4.0
	gopkg.in/yaml.v3 v3.0.1
)
