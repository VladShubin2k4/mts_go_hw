package main

import (
	"hash/fnv"
	"math/rand"
	library "mts_go_hw/task1"
	HashBasedStorage "mts_go_hw/task1/Storage/HashMap"
	SliceBasedStorage "mts_go_hw/task1/Storage/Slice"
	"testing"
)

func GenerateIdByName(title *string) library.ID {
	h := fnv.New32()
	_, err := h.Write([]byte(*title))
	if err != nil {
		panic("Сan not calculate hash!")
	}
	return library.ID(h.Sum32()) % 10000121
}

const (
	HashBased = iota
	SliceBased
)

func usageScenario(t *testing.T, books *[]string, storageT int) {
	asserter := assert.New(t)
	lib := library.Library{}

	switch storageT {
	case HashBased:
		storage := HashBasedStorage.NewStorageHashMap(make(map[library.ID]library.Book))
		lib = library.NewLibrary(&storage, GenerateIdByName)
	case SliceBased:
		base := make([]library.Book, 0)
		storage := SliceBasedStorage.NewStorageSlice(&base)
		lib = library.NewLibrary(&storage, GenerateIdByName)
	}

	for i := 0; i < 5; i += 1 {
		err := lib.AddBook(&library.Book{Title: (*books)[i]})
		if err != nil {
			panic("Something went wrong!")
		}
	}

	for i := 0; i < 2; i += 1 {
		j := rand.Int31n(int32(len(*books)))
		b, err := lib.GiveOutBook(&(*books)[j])
		if err != nil {
			panic("Something went wrong!")
		}
		asserter.Equal(b.Title, (*books)[j], "The two titles should be the same.")
	}
}

func main() {
	books := []string{
		"Thrawn Ascendancy: Chaos Rising",
		"Thrawn Ascendancy: Greater Good",
		"Thrawn Ascendancy: Lesser Evil",
		"Thrawn",
		"Thrawn: Alliances",
		"Thrawn: Treason",
	}
	t := testing.T{}

	usageScenario(&t, &books, HashBased)
	usageScenario(&t, &books, SliceBased)
}
