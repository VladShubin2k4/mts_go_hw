package task1

type Book struct {
	Title     string
	author    string
	genre     string
	publisher string
}
