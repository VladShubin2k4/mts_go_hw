package HashMap

import (
	"errors"
	library "mts_go_hw/task1"
)

type StorageHashMap struct {
	storage map[library.ID]library.Book
}

func NewStorageHashMap(ptr map[library.ID]library.Book) StorageHashMap {
	return StorageHashMap{
		storage: ptr,
	}
}

func (db *StorageHashMap) GetElementById(id library.ID) (library.Book, error) {
	if val, ok := db.storage[id]; ok {
		return val, nil
	}
	return library.Book{}, errors.New("there is no book at this index")
}

func (db *StorageHashMap) AddElementAtIndex(b *library.Book, id library.ID) (err error) {
	err = nil
	if _, ok := db.storage[id]; !ok {
		db.storage[id] = *b
	} else {
		err = errors.New("index collision")
	}
	return
}
