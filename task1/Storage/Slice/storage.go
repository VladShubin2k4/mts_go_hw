package Slice

import (
	"errors"
	library "mts_go_hw/task1"
)

type StorageSlice struct {
	storage []library.Book
}

func NewStorageSlice(arr *[]library.Book) StorageSlice {
	return StorageSlice{
		storage: *arr,
	}
}

func (db *StorageSlice) GetElementById(id library.ID) (library.Book, error) {
	if id < library.ID(len(db.storage)) {
		return db.storage[id], nil
	}
	return library.Book{}, errors.New("index out of range")
}

func (db *StorageSlice) AddElementAtIndex(b *library.Book, id library.ID) (err error) {
	err = nil
	defaultBook := library.Book{}

	if id < library.ID(len(db.storage)) {
		if db.storage[id] != defaultBook {
			err = errors.New("index collision")
		}
	} else {
		for len(db.storage) <= int(id) {
			db.storage = append(db.storage, defaultBook)
		}
	}
	db.storage[id] = *b
	return
}
