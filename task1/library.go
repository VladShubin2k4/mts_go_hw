package task1

type ID uint32

type Container interface {
	GetElementById(id ID) (Book, error)
	AddElementAtIndex(book *Book, id ID) error
}

type Library struct {
	storage     Container
	idGenerator func(name *string) ID
}

func NewLibrary(storage Container, idGenerator func(name *string) ID) Library {
	return Library{
		storage:     storage,
		idGenerator: idGenerator,
	}
}

func (lib *Library) AddBook(b *Book) error {
	return lib.storage.AddElementAtIndex(b, lib.idGenerator(&b.Title))
}

func (lib *Library) GiveOutBook(name *string) (Book, error) {
	return lib.storage.GetElementById(lib.idGenerator(name))
}
